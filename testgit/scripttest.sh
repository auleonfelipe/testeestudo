build-job:
  stage: build
  script:
    - echo "Olá, $GITLAB_USER_LOGIN!"

test-job1:
  stage: test
  script:
    - echo "somente teste"

test-job2:
  stage: test
  script:
    - Eco: "Este trabalho testa algo, mas leva mais tempo do que o test-job1."
    - Eco: "Após a conclusão dos comandos eco, ele executa o comando sleep por 20 segundos"
    - Eco: "o que simula um teste que dura 20 segundos a mais que o test-job1"
    - sleep 20

deploy-prod:
  stage: deploy
  script:
    - echo "Este script realiza a implantação de uma atualização em produção $CI_COMMIT_BRANCH branch."
  environment: production
